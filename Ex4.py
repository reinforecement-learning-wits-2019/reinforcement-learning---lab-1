import numpy as np
from environments.gridworld import GridworldEnv
import time
import pandas as pd
import matplotlib as plt

from lab_1 import *

def main():
    # Create Gridworld environment with size of 5 by 5, with the goal at state 24. Reward for getting to goal state is 0, and each step reward is -1
    dim_1, dim_2 = 5,5
    env = GridworldEnv(shape=[dim_1, dim_2], terminal_states=[24], terminal_reward=0, step_reward=-1)
    state = env.reset()
    print("")
    env.render()
    print("")

    gamma = np.logspace(-0.2, 0, num=30)
    print (gamma)

    elapseTimes = np.zeros(30)


    for gamma_i in range (len(gamma)):
        start = time.time()
        policy, v = policy_iteration(env,discount_factor = gamma_i) # call policy_iteration
        policy, v = value_iteration(env,discount_factor = gamma_i) # call value_iteration
        end = time.time()
        elapseTimes[gamma_i] = end - start

    print (elapseTimes)

    data = pd.DataFrame()

    data['gamma'] = gamma
    data['elapse_times'] = elapseTimes

    print (data)
    fig, ax = plt.subplots()
    ax = data.plot(kind='line',x='gamma',y='elapse_times',color='red')
    plt.grid(True)
    # set axes labels
    ax.set_title('Elapse Times', fontsize=25)
    ax.set_xlabel('Gamma', fontsize=20)
    ax.set_ylabel('Time(seconds)', fontsize=20)
    fig.suptitle('', fontsize=22)

    plt.show()

if __name__ == "__main__":
    main()
